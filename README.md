# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:
```
git clone https://DrakslerT@bitbucket.org/DrakslerT/stroboskop.git
cd stroboskop
```

Naloga 6.2.3:
https://bitbucket.org/DrakslerT/stroboskop/commits/2df9eacfa334a722b1445fdfb9c38000d77adc8b


## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/DrakslerT/stroboskop/commits/4a2b08dcd16bbbba9fa5bc86be9352c24a1d6ee9

Naloga 6.3.2:
https://bitbucket.org/DrakslerT/stroboskop/commits/26960b8fc09d4374b939ebc7b47a7eaa0296da40

Naloga 6.3.3:
https://bitbucket.org/DrakslerT/stroboskop/commits/9abda566d0138b09fa7af3b07bfa565e576a20f5

Naloga 6.3.4:
https://bitbucket.org/DrakslerT/stroboskop/commits/1ec120de1a67a265879e82dac76164ad70c29e39

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/DrakslerT/stroboskop/commits/ca7603bc9403fec3a10dc6df267a661035233f07

Naloga 6.4.2:
https://bitbucket.org/DrakslerT/stroboskop/commits/29e27698cf051c4e34676b66bdcba8bc97afbebe

Naloga 6.4.3:
https://bitbucket.org/DrakslerT/stroboskop/commits/370b5c32a39b3e3e3ddb4b86ee4f9a2eb2ae2b47

Naloga 6.4.4:
https://bitbucket.org/DrakslerT/stroboskop/commits/35e535fb2dd0238fe4819e27b60365c25ee5e2f3